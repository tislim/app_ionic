import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login.page';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  seconnecter(){
    this.router.navigate(['login']);
  }

}
