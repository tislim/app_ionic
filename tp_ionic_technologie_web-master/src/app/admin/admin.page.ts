import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';
import { PoudchDbMildaService } from '../poudch-db-milda.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

  public _id: string;
  id: string;
  public rev: any;
  public _rev: any;
  public editcontrubbuable: any;
  public data: any;
  public contrubuable: any;
  ageographique: any;
  public contrubuableWithAg: any;
  public contrubuableList: any;
  public listContribuable = [];
  public result: any;

  constructor(private pouchDbService: PoudchDbMildaService, private route: ActivatedRoute, private router: Router, private dataService: DataService) { }

  ngOnInit() {
    if(this.route.snapshot.data['special']){
      this.data = this.route.snapshot.data['special'];
    }
  }

  save(){
    this.router.navigateByUrl('/propriete/42');
  }

  milda(){
    this.router.navigate(['fiche-milda']);
  }

  // Need to be verified
  medicament(){
    this.router.navigate(['fiche-medicament']);
  }

  logout(){
    this.router.navigate(['login']);
  }
}
