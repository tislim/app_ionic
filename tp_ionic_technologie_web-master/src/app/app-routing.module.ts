import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { DataService } from './data.service';
import { DataResolverService } from './resolver/data-resolver.service';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'accueil',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'accueil',
    loadChildren: () => import('./accueil/accueil.module').then(m => m.AccueilPageModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminPageModule)
  },
  {
    path: 'photo-gps1',
    loadChildren: () => import('./photo-gps1/photo-gps1.module').then(m => m.PhotoGps1PageModule)
  },
  {
    path: 'photo-gps',
    loadChildren: () => import('./photo-gps/photo-gps.module').then(m => m.PhotoGpsPageModule)
  },
  {
    path: 'fiche-milda',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./fiche-milda/fiche-milda.module').then(m => m.FicheMildaPageModule)
  },
  {
    path: 'fiche-medicament',
    loadChildren: () => import('./fiche-medicament/fiche-medicament.module').then(m => m.FicheMedicamentPageModule)
  },

  {
    path: 'fiche-milda/:id',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./fiche-milda/fiche-milda.module').then(m => m.FicheMildaPageModule)
  },
  {
    path: 'fiche-medicament/:id',
    resolve: {
      special: DataService
    },
    loadChildren: () => import('./fiche-milda/fiche-milda.module').then(m => m.FicheMildaPageModule)
  },
  {
    path: 'admin/:id',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
