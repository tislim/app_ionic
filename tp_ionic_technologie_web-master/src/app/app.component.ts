import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx'
import { PoudchDbMildaService } from './poudch-db-milda.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  providers: [PoudchDbMildaService,StatusBar,SplashScreen],
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  public appPages = [
    {
      title: 'Accueil',
      url: '/accueil',
      icon: 'home'
    },

    {
      title: 'Admin',
      url: '/admin',
      icon: 'list'
    },
    {
      title: 'Login',
      url: '/login',
      icon: 'login'
    }
  ];

  initializeApp(){
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    this.platform.backButton.subscribeWithPriority(9999, () => {
      document.addEventListener('backbutton', function(event){
        event.preventDefault();
        event.stopPropagation();
        console.log('hello');
      }, false);
    });
  }
}
