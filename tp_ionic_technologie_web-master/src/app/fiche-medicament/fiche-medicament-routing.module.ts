import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FicheMedicamentPage } from './fiche-medicament.page';

const routes: Routes = [
  {
    path: '',
    component: FicheMedicamentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FicheMedicamentPageRoutingModule {}
