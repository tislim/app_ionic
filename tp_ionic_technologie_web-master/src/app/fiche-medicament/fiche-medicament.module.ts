import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FicheMedicamentPageRoutingModule } from './fiche-medicament-routing.module';

import { FicheMedicamentPage } from './fiche-medicament.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FicheMedicamentPageRoutingModule
  ],
  declarations: [FicheMedicamentPage]
})
export class FicheMedicamentPageModule {}
