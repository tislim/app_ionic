import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FicheMildaPage } from './fiche-milda.page';

const routes: Routes = [
  {
    path: '',
    component: FicheMildaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FicheMildaPageRoutingModule {}
