import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FicheMildaPageRoutingModule } from './fiche-milda-routing.module';

import { FicheMildaPage } from './fiche-milda.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FicheMildaPageRoutingModule
  ],
  declarations: [FicheMildaPage]
})
export class FicheMildaPageModule {}
