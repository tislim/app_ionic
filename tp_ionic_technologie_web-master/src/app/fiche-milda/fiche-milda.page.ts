import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ToastController } from '@ionic/angular';
import { DataService } from '../data.service';
import { PoudchDbMildaService } from '../poudch-db-milda.service';

@Component({
  selector: 'app-fiche-milda',
  templateUrl: './fiche-milda.page.html',
  styleUrls: ['./fiche-milda.page.scss'],
  providers: [Camera,Geolocation ],
})
export class FicheMildaPage implements OnInit {

  public dataMilda: any;
  collecteMilda = {
    pays: '',
    region: '',
    moughataa: '',
    commune: '',
    village: '',
    complement: '',
    nomdistributeur: '',
    date: '',
    chefMenage: '',
    nni: '',
    contact: '',
    nbMilda: '',
    centre: '',
    latitude: '',
    longitude: '',
    photo: '',
    photoBeneficiaire: ''
  };

  adresse = {
    pays: '',
    region: '',
    moughataa: '',
    commune: '',
    village: '',
    complement: '',
    latitude: '',
    longitude: ''
  };

  team = {
    nomdistributeur: '',
    date: ''
  };

  beneficiaire = {
    chefMenage: '',
    contact: '',
    nbMilda: '',
    nni: '',
    centre: '',
    photoBeneficiaire: ''
  };

  base64Image: any;

  constructor(
    private pouchDBMildaService: PoudchDbMildaService,
    private route: ActivatedRoute,
    private router: Router,
    public toastCtrl: ToastController,
    private _CAMERA: Camera,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.dataMilda = this.route.snapshot.data['special'];
  }

  saveMilda() {
    this.collecteMilda.pays = this.dataMilda.pays;
    this.collecteMilda.region = this.dataMilda.region;
    this.collecteMilda.moughataa = this.dataMilda.moughataa;
    this.collecteMilda.commune = this.dataMilda.complement;
    this.collecteMilda.village = this.dataMilda.village;
    this.collecteMilda.complement = this.dataMilda.complement;
    this.collecteMilda.photoBeneficiaire = this.dataMilda.photoBeneficiaire;
    this.collecteMilda.photo = this.dataMilda.photo;
    this.collecteMilda.nomdistributeur = this.team.nomdistributeur;
    this.collecteMilda.nni = this.beneficiaire.nni;
    this.collecteMilda.nbMilda = this.beneficiaire.nbMilda;
    this.collecteMilda.moughataa = this.dataMilda.moughataa;
    this.collecteMilda.longitude = this.dataMilda.longitude;
    this.collecteMilda.latitude = this.dataMilda.latitude;
    this.collecteMilda.date = this.team.date;
    this.collecteMilda.contact = this.beneficiaire.contact;
    this.collecteMilda.commune = this.dataMilda.commune;
    this.collecteMilda.chefMenage = this.beneficiaire.chefMenage;
    this.collecteMilda.centre = this.beneficiaire.centre;
    this.collecteMilda.photoBeneficiaire = this.beneficiaire.photoBeneficiaire;

    this.pouchDBMildaService.createCollecte(this.collecteMilda).then((data) => {
      this.sendNotification('Collecte sauvegarder avec succee');
      this.resetCollecte();
    });
  }

  sendNotification(message): void {
    let notification = this.toastCtrl.create({
      message: message,
      duration: 1000
    }).then((notification) => {
      notification.present();
    });
  }

  logout() {
    this.router.navigate(['login']);
  }

  resetCollecte() {
    this.collecteMilda.pays = '';
    this.collecteMilda.complement = '';
    this.collecteMilda.village = '';
    this.collecteMilda.commune = '';
    this.collecteMilda.photoBeneficiaire = '';
    this.collecteMilda.photo = '';
    this.collecteMilda.nomdistributeur = '';
    this.collecteMilda.nni = '';
    this.collecteMilda.nbMilda = '';
    this.collecteMilda.moughataa = '';
    this.collecteMilda.longitude = '';
    this.collecteMilda.latitude = '';
    this.collecteMilda.date = '';
    this.collecteMilda.contact = '';
    this.collecteMilda.commune = '';
    this.collecteMilda.chefMenage = '';
    this.collecteMilda.centre = '';
    this.team.date = '';
    this.team.nomdistributeur = '';
    this.beneficiaire.chefMenage = '';
    this.beneficiaire.contact = '';
    this.beneficiaire.nbMilda = '';
    this.beneficiaire.nni = '';
    this.beneficiaire.centre = '';
    this.beneficiaire.photoBeneficiaire = '';

  };

  selectImage() {
    const options: CameraOptions = {
      quality: 50, //picture quality
      destinationType: this._CAMERA.DestinationType.DATA_URL,
      encodingType: this._CAMERA.EncodingType.JPEG,
      mediaType: this._CAMERA.MediaType.PICTURE
    };

    this._CAMERA.getPicture(options)
      .then((data) => {
        this.base64Image = "data:image/jpeg;base64," + data;
        this.beneficiaire.photoBeneficiaire = this.base64Image;
      }, (err) => {
        console.log(err);
      });
  };

  deletePhoto(){
    this.beneficiaire.photoBeneficiaire = '';
  };

}
