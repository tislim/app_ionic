import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private router: Router, private dataService: DataService) { }

  ngOnInit() {
  }

  public email: string;
  public password: string;

  registerCredentials = {
    email: '',
    password: ''
  };

  agent = {
    email: '',
    date_creation: ''
  };

  public login(): any {
    this.email = this.registerCredentials.email;
    this.password = this.registerCredentials.password;

    if ((this.email == 's3a' && this.password == 'stroisa') || (this.email == 'groupements3a' && this.password == 'groupements3a')) {
      this.dataService.setData(42, this.registerCredentials);
      this.router.navigateByUrl('/admin/42');
    }
    else {
      this.registerCredentials.email = "";
      this.registerCredentials.password = "";
    }
  }

}
