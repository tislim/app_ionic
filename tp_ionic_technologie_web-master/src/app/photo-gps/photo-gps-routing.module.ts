import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PhotoGpsPage } from './photo-gps.page';

const routes: Routes = [
  {
    path: '',
    component: PhotoGpsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhotoGpsPageRoutingModule {}
