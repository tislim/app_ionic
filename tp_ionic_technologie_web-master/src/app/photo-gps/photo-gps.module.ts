import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PhotoGpsPageRoutingModule } from './photo-gps-routing.module';

import { PhotoGpsPage } from './photo-gps.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PhotoGpsPageRoutingModule
  ],
  declarations: [PhotoGpsPage]
})
export class PhotoGpsPageModule {}
