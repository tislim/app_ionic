import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PhotoGps1Page } from './photo-gps1.page';

const routes: Routes = [
  {
    path: '',
    component: PhotoGps1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhotoGps1PageRoutingModule {}
