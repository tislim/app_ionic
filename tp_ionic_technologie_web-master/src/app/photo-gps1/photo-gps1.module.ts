import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PhotoGps1PageRoutingModule } from './photo-gps1-routing.module';

import { PhotoGps1Page } from './photo-gps1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PhotoGps1PageRoutingModule
  ],
  declarations: [PhotoGps1Page]
})
export class PhotoGps1PageModule {}
