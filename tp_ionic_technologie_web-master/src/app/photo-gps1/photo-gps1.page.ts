import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { DataService } from '../data.service';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

import { ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-photo-gps1',
  templateUrl: './photo-gps1.page.html',
  styleUrls: ['./photo-gps1.page.scss'],
  providers: [Camera,Geolocation,LocationAccuracy ]
})

export class PhotoGps1Page implements OnInit {

  adresse = {
    pays: '',
    region: '',
    moughataa: '',
    commune: '',
    village: '',
    complement: '',
    latitude: '',
    longitude: ''
  };

  locationCoords: any;

  dataMilda = {
    pays: '',
    region: '',
    moughataa: '',
    commune: '',
    village: '',
    complement: '',
    latitude: '',
    longitude: '',
    photo: ''
  };

  base64Image: any;
  public photo: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private _CAMERA: Camera,
    public androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    public toastCtrl: ToastController,
    public locationAccuracy: LocationAccuracy
  ) {
    this.locationCoords = {
      latitude: '',
      longitude: '',
      accuracy: '',
      timestamp: ''
    };
  }

  ngOnInit() {
  }

  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        }
        else {

          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinates()
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;
      this.locationCoords.accuracy = resp.coords.accuracy;
      this.locationCoords.timestamp = resp.timestamp;
    }).catch((error) => {
      alert('Error getting location ' + error);
    });
  }

  logout() {
    this.router.navigate(['login']);
  }

  suivantMilda() {
    this.dataMilda.pays = this.adresse.pays;
    this.dataMilda.commune = this.adresse.commune;
    this.dataMilda.region = this.adresse.region;
    this.dataMilda.moughataa = this.adresse.moughataa;
    this.dataMilda.village = this.adresse.village;
    this.dataMilda.complement = this.adresse.complement;
    this.dataMilda.latitude = this.locationCoords.latitude;
    this.dataMilda.longitude = this.locationCoords.longitude;
    this.dataMilda.photo = this.photo;
    this.dataService.setData(2, this.dataMilda);
    this.router.navigateByUrl('/fiche-milda/2');
  }

  selectImage() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this._CAMERA.DestinationType.DATA_URL,
      encodingType: this._CAMERA.EncodingType.JPEG,
      mediaType: this._CAMERA.MediaType.PICTURE
    };

    this._CAMERA.getPicture(options)
      .then((data) => {
        this.base64Image = "data:image/jpeg;base64," + data;
        this.photo = this.base64Image;
      }, (err) => {
        console.log(err);
      });

  }

  deletePhoto() {
    this.photo = '';
  }

}
