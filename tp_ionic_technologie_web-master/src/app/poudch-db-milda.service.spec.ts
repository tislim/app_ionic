import { TestBed } from '@angular/core/testing';

import { PoudchDbMildaService } from './poudch-db-milda.service';

describe('PoudchDbMildaService', () => {
  let service: PoudchDbMildaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PoudchDbMildaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
