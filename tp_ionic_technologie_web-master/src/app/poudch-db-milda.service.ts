import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';

@Injectable({
  providedIn: 'root'
})
export class PoudchDbMildaService {

  public db: any;
  public remote: any;
  public data: any;
  public success: boolean = true;

  constructor() {
    this.db = new PouchDB('medicament');
    this.remote = 'http://192.168.0.101.5984/medicament';
    let options = {
      live: true,
      retry: true,
      continuous: true
    };
    this.db.replicate.to(this.remote, options);
    //this.db.sync(this.remote, options);

  }

  handleChange(change){

    let changeDoc = null;
    let changeIndex = null;

    this.data.forEach((doc, index) => {
      
      if(doc._id === change.id){
        changeDoc = doc;
        changeIndex = index;
      }
      
    });

    //A document was deleted
    if(change.deleted){
      this.data.splice(changeIndex, 1);
    }
    else{
      //A document was updated
      if(changeDoc){
        this.data[changeIndex] = change.doc;
      }
      //A document was added
      else{
        this.data.push(change.doc);
      }
    }
  }

  createCollecte(collecteMidla){
    return new Promise(resolve => {
      this.db.post(collecteMidla).catch((err) => {
        this.success = false;
      });
      resolve(true);
      this.db.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
        this.handleChange(change);
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  updateCollecteMilda(collecteMidla){
    return new Promise(resolve => {
      this.db.put(collecteMidla).cache((err)=>{
        this.success=false;
      });
      resolve(true);
      this.db.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
        this.handleChange(change);
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  removeCollecteMILDA(collecteMidla){
    return new Promise(resolve => {
      
      this.db.remove(collecteMidla).catch((err) => {
        this.success = false;
      });

      if(this.success){
        resolve(true);
      }
    });
  }

  getDb(){
    return this.db;
  }
}
